
export const state = () => ({
    mainDrawerEnabled: true,
});

export const mutations = {
    SET_MAIN_DRAWER_ENABLED: (state, drawerFlag) => {
        state.mainDrawerEnabled = drawerFlag;
    },
};

export const actions = {
    DISABLE_DRAWER: ({commit}) => {
        commit("SET_MAIN_DRAWER_ENABLED", false);
    },
    ENABLE_DRAWER: ({commit}) => {
        commit("SET_MAIN_DRAWER_ENABLED", true);
    },
};
